## 0.19.0-rc1 (2024-05-15)

### Added (1 change)

- [Document best practice for lockfile handling](components/opentofu@e317401e6b3d8cb1b3b7f8f341eb6d3ab046961a) ([merge request](components/opentofu!64))

### Fixed (1 change)

- [Mock CI_SERVER_HOST predefined env variable for unit test](components/opentofu@1e5b1e3c269d4a38c986bdfec8dbd69456de79c2) ([merge request](components/opentofu!65))

## 0.18.0-rc5 (2024-04-08)

No changes.

## 0.18.0-rc4 (2024-04-08)

No changes.

## 0.18.0-rc3 (2024-04-08)

No changes.

## 0.18.0-rc2 (2024-04-08)

No changes.
